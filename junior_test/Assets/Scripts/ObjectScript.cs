﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEditor;

namespace Test
{

    public class ObjectScript : MonoBehaviour
    {
        #region
        private MeshRenderer _mesh;
        private int _countClick = 0;
        private List<Color> _colors = new List<Color>();
        private bool isRandomColor = true;
        private IDisposable _timer;
        private ClickColorData _colorData;
        private int _colorIndex ;
        private int _observableTime;
        private string _name;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            _observableTime = _observableTime > 0 ? _observableTime * 1000 : 1000;

             _colorData  = AssetDatabase.LoadAssetAtPath<ClickColorData>("assets/Prefabs/ClickColorData/" + _name + "Data.asset");

            _mesh = GetComponent<MeshRenderer>();


            _timer = Observable.Interval(TimeSpan.FromMilliseconds(_observableTime)).Subscribe(X =>
                {
                    ChangeColor();
                });
        }

        // Update is called once per frame
        void Update()
        {

        }
        private void ChangeColor()
        {
            if (_colors.Count > 0)
            {
                if (_colorIndex >= _colors.Count)
                {
                    _colorIndex = 0;
                }
                _mesh.material.color = _colors[_colorIndex];

                _colorIndex++;
            }
            else
            {
                _mesh.material.color = new Color(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f));

            }
        }
        #endregion
        #region public methods,properties
        public void Click()
        {
            _countClick++;
            _colors.Clear();
            foreach( var elem in _colorData.ClicksData)
            {
                if (_countClick>= elem.minCountClick && _countClick <= elem.maxCountClick)
                {
                    _colors.Add(elem.color);
                }
            }

        }
        public int ObservableTime
        {
            get { return _observableTime; }
            set { _observableTime = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        #endregion
    }
}