﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test
{
[CreateAssetMenu(menuName ="Test/Game data", fileName ="GameData")]
    public class GameData : ScriptableObject
    {
        #region private fields
        [SerializeField] private int _observableTime;
        #endregion

        #region public methods, properties
        public int ObservableTime
        {
            get { return _observableTime; }
        }
        
        #endregion
    }
}