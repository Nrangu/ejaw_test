﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Test
{
    [Serializable]
    public struct Elem
    {
        public int minCountClick;
        public int maxCountClick;
        public Color color;
    }
    [CreateAssetMenu(menuName = ("Test/Click color data"), fileName = ("ClickColorData"))]
    public class ClickColorData : ScriptableObject
    {
        #region private fields
        [SerializeField] List<Elem> _clicksData;

        #endregion
        #region public properties
        public List<Elem> ClicksData
        {
            get { return _clicksData; }
        }
        #endregion
    }
}