﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
namespace Test
{
[System.Serializable]
public class Names
    {
        //employees is case sensitive and must match the string "employees" in the JSON.
        public List<string> names = new List<string>();
    }
    public class Main : MonoBehaviour
    {


        #region pivate fields
        private GameData _gameData;
        private bool _mouseDown = false;

        private Names _objectsNames;
        private AssetBundle  _geometricobjects;
        #endregion
        #region
        // Start is called before the first frame update
        void Start()
        {
            // сделать загрузку Gamedata
            _gameData = AssetDatabase.LoadAssetAtPath<GameData>("assets/Prefabs/GameData/GameData.asset");
            string path = Path.Combine(Application.dataPath, "AssetBundles/geometricobjects");
             _geometricobjects = AssetBundle.LoadFromFile(path);

            var jsonTextFile = Resources.Load<TextAsset>("ObjectsNames");
            _objectsNames = JsonUtility.FromJson<Names>( jsonTextFile.text);
        }

        // Update is called once per frame
        void Update()
        {

            if (Input.GetMouseButtonDown(0) && !_mouseDown)
            {
                _mouseDown = true;
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    Transform objectHit = hit.transform;
                    objectHit.GetComponent<ObjectScript>().Click();
                    Debug.Log("Selected");
                }
                else
                {
                    Debug.Log("Not selected");

                    int objectNumber = Random.Range(0, _objectsNames.names.Count);
                    string objectName = _objectsNames.names[objectNumber];
                    var prefab = _geometricobjects.LoadAsset<GameObject>(objectName);

                    Vector3 mousePos = Input.mousePosition;
                    mousePos.z = Mathf.Abs(Camera.main.transform.position.z / 2);

                    Vector3 position = Camera.main.ScreenToWorldPoint(mousePos);
                    GameObject obj = Instantiate(prefab, position, Quaternion.identity);

                    obj.AddComponent<ObjectScript>();
                    obj.GetComponent<ObjectScript>().ObservableTime = _gameData.ObservableTime;
                    obj.GetComponent<ObjectScript>().Name = objectName;
                }
            }
                // Do something with the object that was hit by the raycast.
            
            if (Input.GetMouseButtonUp(0))
            {
                _mouseDown = false;
            }
        }
        #endregion
    }
}